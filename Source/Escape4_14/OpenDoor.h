// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Grabber.h"
#include "OpenDoor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPE4_14_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;
    
    UPROPERTY(BluePrintAssignable)
    FDoorEvent OnOpen;
    
    
    UPROPERTY(BluePrintAssignable)
    FDoorEvent OnClose;

private:
    UPROPERTY(EditAnywhere)
    bool OpensToInside;
    
    UPROPERTY(EditAnywhere)
    ATriggerVolume* Trigger = nullptr;
    
    UPROPERTY(EditAnywhere)
    float TriggerMass = 30.f;
	
    float GetMassInKgOnTrigger();
    
    UGrabber* Grabber = nullptr;
};
