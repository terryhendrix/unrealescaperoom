// Fill out your copyright notice in the Description page of Project Settings.

#include "Escape4_14.h"
#include "OpenDoor.h"
#include "Grabber.h"

#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
    
    Grabber = GetWorld()->GetFirstPlayerController()->GetPawn()->FindComponentByClass<UGrabber>();
    
    if(!Grabber) {
        UE_LOG(LogTemp, Error, TEXT("%s could not find UGrabber linked to first player controller, this has impact on mass calculation."), *GetOwner()->GetName());
    }
    
    if(!Trigger) {
        UE_LOG(LogTemp, Error, TEXT("%s missing Trigger."), *GetOwner()->GetName());
    }
}

// Called every frame
void UOpenDoor::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
    
    if(GetMassInKgOnTrigger() > TriggerMass) {
        UE_LOG(LogTemp, Error, TEXT("Broadcast open event."));
        OnOpen.Broadcast();
    }
    else {
        OnClose.Broadcast();
    }
}

float UOpenDoor::GetMassInKgOnTrigger() {
    float TotalMass = 0.f;
    TSet<AActor *> ActorsOnPlate;
    
    if(!Trigger) { return 0.0f; };
    Trigger->GetOverlappingActors(OUT ActorsOnPlate);
    
    for(const auto& Actor : ActorsOnPlate) {
        auto Primitive = Actor->FindComponentByClass<UPrimitiveComponent>();
        
        // Only take weight into account that is currently not lifted. Makes no sense to add this weight to the trigger
        if(Grabber == nullptr || !Grabber->IsGrabbing(Primitive)) {
            UE_LOG(LogTemp, Warning, TEXT("%s is on trigger"), *Actor->GetName());
            TotalMass += Primitive->GetMass();
        }
    }
    
    return TotalMass;
}






