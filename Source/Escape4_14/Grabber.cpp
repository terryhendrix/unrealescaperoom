// Fill out your copyright notice in the Description page of Project Settings.

#include "Escape4_14.h"
#include "Grabber.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
    UE_LOG(LogTemp, Warning, TEXT("Starting grabber")); 
    FindPhysicsHandleComponent();
    SetupInputComponent();
}

void UGrabber::FindPhysicsHandleComponent() {
    // Look for attached UPhysicsHandleComponent
    PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
    if(PhysicsHandle == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("%s missing PhysicsHandleComponent"), *GetOwner()->GetName());
    }
}

void UGrabber::SetupInputComponent() {
    // Look for attached UInputComponent
    Input = GetOwner()->FindComponentByClass<UInputComponent>();
    if(Input) {
        // Bind the input axis
        Input->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
        Input->BindAction("Grab", IE_Released, this, &UGrabber::Release);
        
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("%s missing InputComponent"), *GetOwner()->GetName());
    }
}

void UGrabber::Grab() {
    auto Hit = GetFirstPhysicsBodyInReach();
    auto ActorHit = Hit.GetActor();
    auto ComponentToGrab = Hit.GetComponent();
    
    if(ActorHit) {
        UE_LOG(LogTemp, Warning, TEXT("%s grabs %s"), *GetOwner()->GetName(), *ActorHit->GetName());
        // attach physics handle
        OriginalGrabbedRotation = ActorHit->GetActorRotation();
        OriginalPlayerRotation = GetViewPointRotation();
        
        if(!PhysicsHandle) { return; }
        PhysicsHandle->GrabComponentAtLocationWithRotation(
            ComponentToGrab,
            NAME_None,
            ComponentToGrab->GetOwner()->GetActorLocation(),
            OriginalGrabbedRotation
        );
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("%s grabs nothing"), *GetOwner()->GetName());
    }
}

void UGrabber::Release() {
    UE_LOG(LogTemp, Warning, TEXT("Release"));
    // detach physics handle
    if(!PhysicsHandle) { return; }
    PhysicsHandle->ReleaseComponent();
}


bool UGrabber::IsGrabbing(UPrimitiveComponent* Component) {
    if(!PhysicsHandle) { return false; }
    return PhysicsHandle->GrabbedComponent == Component;
}

FVector UGrabber::GetViewPointLocation() {
    FVector Location;
    FRotator Rotation;
    GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT Location, OUT Rotation);
    return Location;
}

FRotator UGrabber::GetViewPointRotation() {
    FVector Location;
    FRotator Rotation;
    GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT Location, OUT Rotation);
    return Rotation;
}

FVector UGrabber::GetReachLineStart() {
    return GetViewPointLocation();
}

FVector UGrabber::GetReachLineEnd() {
    return GetViewPointLocation() + GetViewPointRotation().Vector() * Reach;
}

const FHitResult UGrabber::GetFirstPhysicsBodyInReach() {
    auto LineTraceEnd = GetReachLineEnd();
    
    /// Ray cast (aka line trace)
    FHitResult Hit;
    FCollisionQueryParams SimpleTraceParams = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
    FCollisionObjectQueryParams Query = FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody);
    GetWorld()->LineTraceSingleByObjectType(OUT Hit, GetReachLineStart(), LineTraceEnd, Query, SimpleTraceParams);
    
    return Hit;
}

// Called every frame
void UGrabber::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
    
    if(!PhysicsHandle) { return; }
    
    // if physics handle is attached, move the location
    if(PhysicsHandle->GrabbedComponent) {
        auto LineTraceEnd = GetReachLineEnd();
        PhysicsHandle->SetTargetLocation(LineTraceEnd);
        PhysicsHandle->SetTargetRotation(OriginalGrabbedRotation + FRotator(0, (GetViewPointRotation() - OriginalPlayerRotation).Yaw, 0));
    }
    
}

