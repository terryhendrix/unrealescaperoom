// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPE4_14_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

    bool IsGrabbing(UPrimitiveComponent* Component);
    
private:
    UPROPERTY(EditAnywhere)
    float Reach = 100.f;
	
    UPhysicsHandleComponent* PhysicsHandle = nullptr;
    
    UInputComponent* Input = nullptr;
    
    FRotator OriginalGrabbedRotation;
    
    FRotator OriginalPlayerRotation;
    
    void FindPhysicsHandleComponent();
    
    void SetupInputComponent();
    
    FVector GetViewPointLocation();
    
    FRotator GetViewPointRotation();
    
    FVector GetReachLineEnd();
    
    FVector GetReachLineStart();
    
    // Ray cast and grab what is in reach
    void Grab();
    
    // Release whatever your grabbing on to
    void Release();
    
    const FHitResult GetFirstPhysicsBodyInReach();
};
